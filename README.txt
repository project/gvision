GVISION
----------------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Requirements
 * Installation
 * Configuration
 * Future developments
 * Contributions are welcome!!


INTRODUCTION
------------

The GVision module provides integration with the Google Vision API using the
Google PHP Client Library through two blocks, where in one you can choose
which API Function to use when uploading the image and in the other you
pre-select the function via block configuration form. Although this being
a simple request->return->display process via ajax, the main proposal of
the module goes around the hook that it makes available. Through it you
can intercept the response and do whatever you want with the results.


FEATURES
--------

First, a few concrete use cases for which this module will certainly come in
handy:
 * Fill a search form through the text from an image.
 * Detect logos from images and process the information
 * Redirect to any page based on image properties, face detection, text in the
   image, etc.
 * Label images based on its content.
 * Landmark images based on its content.

Gvision can be configured globally through an admin settings form ,
allowing users to define their Gooel API Key, the max results that the API
should return and the max file size of the uploaded images. You can also
configure the single function block through the block configure form.

Through GVision's hook, developers have the flexibility intercept the API
response and do whatever has to be done with it, before it goes back to the
form and be displayed.

A good example is a case where you have a textfield for end users to fill,
and you send the inserted value to a WebService, get the responses and show
something that the webservice returned to the user. You can then use one of
the blocks in the same page as the textfield to give the users the option of
uploading the image, and for example, using the OCR function to get the text
directly from the image, then with the hook you can intercept the result,
send to your custom functions of processing,requesting, etc. and then redirect
the user to the result page.



REQUIREMENTS
------------

This module requires the following modules:

 * Google PHP Client Library (https://www.drupal.org/sandbox/davic/2779027)
 * Libraries (https://www.drupal.org/project/libraries)
 * File


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

 * Customize the general API request parameters (such as number of
responses allowed per request) in Administration >> Configuration >>
Services >> GVision.
   
   - The API Key field relates to your Google Console API Key.

   - The Max Results field expects the number of results allowed per
request. This number of request may not be fulfilled every time because
the API returns as many results as it can understand based on the image.

   - The Max File Size field is the max file size allowed for the request 
to handle. It has a cap of 4194304 bytes.

 * When using the single API function block, you can also configure it
through the block configuration page in Administration >> Structure >>
Blocks >> Click "Configure" for the Google Vision API Single Function Block.


FUTURE DEVELOPMENTS
-------------------

The next idea is to implement a widget for the Image field that does the
statement processing as the blocks, and together with it create a new hook for
intercepting it just like the blocks.


CONTRIBUTIONS ARE WELCOME!!
---------------------------

Feel free to follow up in the issue queue for any contributions, bug
reports, feature requests.
Tests, feedback or comments in general are highly appreciated.
