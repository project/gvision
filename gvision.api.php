<?php

/**
 * @file
 * Hooks provided by Drupal core and the System module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Get the value returned from the api and the function used.
 *
 * @param string $value
 *   The value returned from the API request.
 * @param string $function
 *   Contains the API function that was used in the image and
 *   returned the $value.
 *
 *   This function have no return, everything that has to be done
 *   with the returned value should happen here.
 */
function hook_gvision_block($value, $function) {

  // So here we suppose the processing expects the read string to be
  // following a format like "DDTEST-<code>", so we check for it
  // and if it isn't found, we add it manually before sending for processing.
  if (!strpos($value)) {
    $value = "DDTEST-" . $value;
  }

  // Feed the array of data to be passed to the results page.
  $parameters = array(
    'value' => $value,
  );

  // Redirect to the results page.
  drupal_goto('results-page', array('query' => $parameters));
}

/**
 * @} End of "addtogroup hooks".
 */
