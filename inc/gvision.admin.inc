<?php

/**
 * @file
 * Gvision Module Settings Form.
 */

/**
 * Form for setting up request settings.
 */
function gvision_settings_form($form, &$form_state) {
  $form = array();

  $form['gvision_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Cloud API Key'),
    '#description' => t('A Server API key generated through your project at Google Cloud Console. (See
                      reference: <a href="https://support.google.com/cloud/answer/6158862?hl=en">
                      https://support.google.com/cloud/answer/6158862?hl=en</a>).'),
    '#required' => TRUE,
    '#default_value' => variable_get('gvision_api_key'),
  );

  $form['gvision_max_results'] = array(
    '#type' => 'textfield',
    '#title' => t('Max Results'),
    '#description' => t('Max Results allowed per request(1 is recommended for most cases). The API might
                      return less results.'),
    '#required' => TRUE,
    '#default_value' => variable_get('gvision_max_results', 1),
  );

  $form['gvision_max_file_size'] = array(
    '#type' => 'textfield',
    '#title' => t('Max Uploaded File Size'),
    '#description' => t('Max file size allowed per request in bytes. Defaults
                      to 4MB as said in the API docs and cannot exceed this value.'),
    '#required' => TRUE,
    '#default_value' => variable_get('gvision_max_file_size', 4194304),
  );

  $form['#submit'][] = 'gvision_settings_submit';
  $form['#validate'][] = 'gvision_settings_validate';

  return system_settings_form($form);
}

/**
 * Implements custom validation for the settings form.
 */
function gvision_settings_validate($form, &$form_state) {
  if (!ctype_digit($form_state['values']['gvision_max_results'])) {
    form_set_error('gvision_max_results', t('The Max Results field must contain only numbers.'));
  }

  if (!ctype_digit($form_state['values']['gvision_max_file_size'])) {
    form_set_error('gvision_max_file_size', t('The Max File Size field must contain only numbers.'));
  }

  if ($form_state['values']['gvision_max_file_size'] > 4194304) {
    form_set_error('gvision_max_file_size', t('The Max File Size must not exceed 4MB.'));
  }
}

/**
 * Implements custom submit for the settings form.
 */
function gvision_settings_submit($form, &$form_state) {
  // Save our configuration values in the variables for later use.
  variable_set('gvision_api_key', $form_state['values']['gvision_api_key']);
  variable_set('gvision_max_results', $form_state['values']['gvision_max_results']);
  variable_set('gvision_max_file_size', $form_state['values']['gvision_max_file_size']);
}
